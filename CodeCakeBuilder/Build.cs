using Cake.Common.Diagnostics;
using Cake.Common.IO;
using Cake.Common.Solution;
using Cake.Common.Tools.MSBuild;
using Cake.Core;
using Cake.Core.Diagnostics;
using SimpleGitVersion;
using System;
using System.Linq;

namespace CodeCake
{

    public partial class Build : CodeCakeHost
    {

        public Build()
        {
            Cake.Log.Verbosity = Verbosity.Diagnostic;

            var solutionFileName = Cake.Environment.WorkingDirectory.GetDirectoryName() + ".sln";

            var projects = Cake.ParseSolution( solutionFileName )
                                       .Projects
                                       .Where( p => !(p is SolutionFolder) && p.Name != "CodeCakeBuilder" );

            // We do not generate NuGet packages for /Tests projects for this solution.
            var projectsToPublish = projects
                                        .Where( p => !p.Path.Segments.Contains( "Tests" ) );

            StandardGlobalInfo globalInfo = CreateStandardGlobalInfo()
                                                 .AddDotnet()
                                                 .SetCIBuildTag();

            Task( "Check-Repository" )
                .Does( () =>
                {
                   // No artefact like NuGet or NPM.
                   // CKSetup artefact are not managed the same way (but they should be).
                   // globalInfo.TerminateIfShouldStop();
               } );

            Task( "Clean" )
                .IsDependentOn( "Check-Repository" )
                .Does( () =>
                 {
                     globalInfo.GetDotnetSolution().Clean();
                     Cake.CleanDirectories( globalInfo.ReleasesFolder.Path );

                 } );

            Task( "Build" )
                .IsDependentOn( "Check-Repository" )
                .IsDependentOn( "Clean" )
                .Does( () =>
                 {
                     globalInfo.GetDotnetSolution().Build();
                 } );

            Task( "Unit-Testing" )
                .IsDependentOn( "Build" )
                .WithCriteria( () => Cake.InteractiveMode() == InteractiveMode.NoInteraction
                                     || Cake.ReadInteractiveOption( "RunUnitTests", "Run Unit Tests?", 'Y', 'N' ) == 'Y' )
               .Does( () =>
                 {
                     if( System.IO.Directory.Exists( "CKSetupRemoteStore/Store" ) )
                     {
                         Cake.Information( "Deleting existing CKSetupRemoteStore/Store." );
                         System.IO.Directory.Delete( "CKSetupRemoteStore/Store", true );
                     }
                     var testProjects = projects.Where( p => p.Name.EndsWith( ".Tests" ) );
                     globalInfo.GetDotnetSolution().Test();
                 } );


            // This task can be run manually:
            //
            //      -target=Build-And-Push-CKRemoteStore-WebSite
            //
            // This task does the build.
            // The appsettings.json is not included in the project.
            // (the <ExcludeFilesFromDeployment>appsettings.json</ExcludeFilesFromDeployment> in
            // pubxml seems to be ignored).
            Task( "Build-And-Push-CKRemoteStore-WebSite" )
                .IsDependentOn( "Unit-Testing" )
                .WithCriteria( () => globalInfo.IsValid )
                .Does( () =>
                {
                    var publishPwd = Cake.InteractiveEnvironmentVariable( "PUBLISH_CKSetupRemoteStore_PWD" );
                    if( !String.IsNullOrWhiteSpace( publishPwd ) )
                    {
                        var conf = new MSBuildSettings();
                        conf.Configuration = globalInfo.BuildInfo.BuildConfiguration;
                        conf.Targets.Add( "Build" );
                        conf.Targets.Add( "Publish" );
                       // To deploy on cksetup.invenietis.net.
                       conf.WithProperty( "DeployOnBuild", "true" )
                            .WithProperty( "PublishProfile", "CustomProfile" )
                            .WithProperty( "UserName", "ci@invenietis.net" )
                            .WithProperty( "Password", publishPwd );
                        conf.AddVersionArguments( globalInfo.BuildInfo );

                        Cake.MSBuild( "CKSetupRemoteStore/CKSetupRemoteStore.csproj", conf );
                    }
                } );

            // The Default task for this script can be set here.
            Task( "Default" )
                .IsDependentOn( "Build-And-Push-CKRemoteStore-WebSite" );
        }
    }
}
