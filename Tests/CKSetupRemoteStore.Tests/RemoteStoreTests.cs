using CK.AspNet.Tester;
using CKSetup;
using CKSetup.StreamStore;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml.Linq;
using static CK.Testing.MonitorTestHelper;

namespace CKSetupRemoteStore.Tests
{
    [TestFixture]
    public class RemoteStoreTests
    {
        static readonly Uri appUri = new Uri( "http://localhost:2982" );

        [Test]
        public async Task pushing_components_and_files_and_downloading_filesAsync()
        {
            TestHelper.CleanupFolder( TestHelper.SolutionFolder.AppendPart( "CKSetupRemoteStore" ).AppendPart( "Store" ), ensureFolderAvailable: false );
            var app = new ExternalProcess( pI =>
            {
                var dir = TestHelper.SolutionFolder.Combine( "CKSetupRemoteStore" );
                pI.WorkingDirectory = dir;
                pI.EnvironmentVariables.Add( "ASPNETCORE_URLS", appUri.ToString() );
                pI.FileName = dir.Combine( $"bin/{TestHelper.BuildConfiguration}/net6.0/CKSetupRemoteStore.exe" );
            } );
            app.EnsureRunning();
            try
            {
                await TestHelper.WithWeakAssemblyResolver( async () =>
                {
                    var sharedHttpClient = new HttpClient();

                    using( IRemoteStore remote = ClientRemoteStore.Create( TestHelper.Monitor, appUri, "HappyKey", () => sharedHttpClient ) )
                    {
                        ComponentDB db1 = CreateInitialDB();
                        PushComponentsResult push = remote.PushComponents( TestHelper.Monitor, w => db1.Export( _ => true, w ) );
                        push.ErrorText.Should().BeNull();
                        push.SessionId.Should().NotBeNull();
                        push.Files.Should().HaveCount( 4, "Model files are not stored." );

                        for( int iFile = 4; iFile <= 7; ++iFile )
                        {
                            remote.PushFile(
                                    TestHelper.Monitor,
                                    push.SessionId,
                                    SampleFiles.Samples[iFile].SHA1,
                                    w =>
                                    {
                                        using( var f = System.IO.File.OpenRead( SampleFiles.Samples[iFile].Path ) )
                                        {
                                            f.CopyTo( w );
                                        }
                                    },
                                    CompressionKind.None ).Should().BeTrue();
                        }

                        for( int iFile = 4; iFile <= 7; ++iFile )
                        {
                            StoredStream s = remote.GetDownloadStream( TestHelper.Monitor, SampleFiles.Samples[iFile].SHA1, CompressionKind.GZiped );
                            s.Kind.Should().Be( CompressionKind.GZiped );
                            s.Stream.Should().NotBeNull();
                            await s.Stream.DisposeAsync();
                        }

                        var dlZipUri = new Uri( appUri, $"/dl-zip/E1/NetCoreApp20" );
                        using( HttpResponseMessage r = await sharedHttpClient.GetAsync( dlZipUri ) )
                        {
                            r.StatusCode.Should().Be( System.Net.HttpStatusCode.OK );
                        }

                    }
                } );
            }
            finally
            {
                app.StopAndWaitForExit();

            }
        }

        static ComponentDB CreateInitialDB()
        {
            var db =
$@"<DB Version=""1"">
    <Component Version=""1.0.0"" Name=""M1"" TargetFramework=""NetStandard20"" Kind=""Model"" >
        <Dependencies>
            <Dependency Version=""1.0.1"" Name=""E1"" />
        </Dependencies>
        <EmbeddedComponents />
        {SampleFiles.XmlFilesFor( 1, 2, 3 )}
    </Component>
    <Component Version=""1.0.1"" Name=""E1"" TargetFramework=""NetCoreApp20"" Kind=""SetupDependency"" >
        <EmbeddedComponents />
        {SampleFiles.XmlFilesFor( 4, 5, 6, 7 )}
    </Component>
    </DB>
";
            return new ComponentDB( XElement.Parse( db ) );
        }

    }
}
