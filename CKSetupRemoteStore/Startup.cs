using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using CK.Core;
using Microsoft.Extensions.Configuration;
using System.Reflection;
using CSemVer;
using CKSetup;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;

namespace CKSetupRemoteStore
{
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
    public class Startup
    {
        public Startup( IConfiguration conf )
        {
            Configuration = conf;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices( IServiceCollection services )
        {
            services.AddOptions();
            services.Configure<KestrelServerOptions>( c => c.AllowSynchronousIO = true );
            services.Configure<IISServerOptions>( c => c.AllowSynchronousIO = true );

            // Store Management.
            services.Configure<CKSetupStoreOptions>( Configuration.GetSection( "store" ) );
            services.AddMemoryCache();
            services.AddSingleton<ComponentDBProvider>();
        }

        public void Configure( IApplicationBuilder app, IWebHostEnvironment env )
        {
            var monitor = new ActivityMonitor( "Pipeline configuration." );

            if( env.IsDevelopment() )
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseGuardRequestMonitor();

            // End with the Store management.
            app.UseMiddleware<CKSetupStoreMiddleware>();
            app.Run( async ( context ) =>
            {
                var dbInfo = context.RequestServices.GetRequiredService<ComponentDBProvider>().Info;
                var a = (AssemblyInformationalVersionAttribute) Attribute.GetCustomAttribute( Assembly.GetExecutingAssembly(), typeof( AssemblyInformationalVersionAttribute ) );
                var v = new InformationalVersion( a?.InformationalVersion );
                await context.Response.WriteAsync(
                    $@"<html>
                        <head>
                            <style>
                                table {{ border-collapse: collapse; }}
                                td, th {{ border-left: solid 1px black; padding: 0px 21px; }}
                                td:first-child, th:first-child{{ border-left: none; }}
                            </style>
                        </head>
                        <body>
                            <h1>Welcome to { env.ApplicationName }.</h1>
                            Version: { v }.<br>

                            <hr><h2>Statistics</h2>
                            - { dbInfo.NamedComponentCount } named components.<br>
                            - { dbInfo.TotalComponentCount } versioned components covering { dbInfo.TotalComponentCountPerFramework.Count } frameworks:<br>
                            { WriteComponentsPerFramework( dbInfo.TotalComponentCountPerFramework ) }
                            - Stored Files: { dbInfo.StoredFilesCount } - { dbInfo.StoredTotalFilesSize / 1024 } KiB <br>
                            - Without file sharing: { dbInfo.ComponentsFilesCount } - { dbInfo.ComponentsTotalFilesSize / 1024 } KiB <br>
                            { WriteFiles( dbInfo.BiggestFiles, "Biggest" ) }<br>
                            <hr>
                            Note:<br>
                            - Direct download component link: <strong>/dl-zip/ComponentName/Runtime/Version</strong> where the version part is optional.
                        </body>
                    </html>" );

                static string WriteComponentsPerFramework( IReadOnlyDictionary<TargetFramework, int> dictionary )
                {
                    if( dictionary.Count == 0 ) return string.Empty;
                    return dictionary.Aggregate( string.Empty, ( current, fc ) => current + $"&nbsp;&nbsp;- {fc.Key} ({fc.Value} versioned components).<br>" );
                }

                static string WriteFiles( IReadOnlyCollection<ComponentFile> files, string title )
                {
                    if( files.Count == 0 ) return string.Empty;
                    return files.Take( 4 ).Aggregate( $"- {title} files:<br>", ( current, f ) => current + $"&nbsp;&nbsp;- {f.ToDisplayString()}<br>" );
                }

            } );

            monitor.MonitorEnd();
        }

    }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
}
