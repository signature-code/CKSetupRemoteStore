using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.IO;
using Microsoft.Extensions.Hosting;
using System.Threading.Tasks;

namespace CKSetupRemoteStore
{
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
    public static class Program
    {
        public static Task Main( string[] args )
        {
            var host = CreateBuilder( args ).Build();
            return host.RunAsync();
        }

        static IHostBuilder CreateBuilder( string[] args )
        {
            var builder = new HostBuilder()
                            .UseCKMonitoring()
                            .ConfigureWebHostDefaults( webHostBuilder =>
            {
                webHostBuilder.UseIISIntegration()
                              .UseContentRoot( Directory.GetCurrentDirectory() )
                              .ConfigureAppConfiguration( ( hostingContext, config ) =>
                              {
                                  config.AddJsonFile( "appsettings.json", optional: false, reloadOnChange: true );
                                  config.AddEnvironmentVariables();
                                  if( args != null ) config.AddCommandLine( args );
                              } )
                              .ConfigureLogging( b => b.AddConsole() )
                              .UseDefaultServiceProvider( ( context, options ) =>
                              {
                                  options.ValidateScopes = context.HostingEnvironment.IsDevelopment();
                              } )
                              .UseStartup<Startup>();
                if( args != null )
                {
                    webHostBuilder.UseConfiguration( new ConfigurationBuilder().AddCommandLine( args ).Build() );
                }
            } );
            return builder;
        }
    }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
}
